package joc;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ClonarTest {

	private char [][] te;
	private char [][] ts;
	

	
	private static char [] [] [] entrada = { 
										{ {'-','-','-','-'},
										  {'*','*','-','*'},
										  {'-','-','-','-'},
										  {'*','*','-','*'}
										},
										{ {'-','*','*','*'},
										  {'*','*','-','*'}
										},
										{ {'-','-','-','-'},
										  {'*','*','-','*'},
										  {'-','-','-','-'},
										  {'*','*','-','*'}
										}  
								};
	private static char [] [] [] sortida = { 
											{ {'-','-','-','-'},
											  {'*','*','-','*'},
											  {'-','-','-','-'},
											  {'*','*','-','*'}
											},
											{ {'-','*','*','*'},
											  {'*','*','-','*'}
											},
											{ {'-','-','-','-'},
											  {'*','*','-','*'},
											  {'-','-','-','-'},
											  {'*','*','-','*'}
											} 
										};
	
	public ClonarTest (char [][] t1,char [][] t2) {
		this.te = t1;
		
		this.ts = t2;
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		
		return Arrays.asList(new Object[][] {
			{entrada[0],  sortida[0]},
			{entrada[1], sortida[1]},
			{entrada[2],  sortida[2]}
			
		});
		
	}
	

	@Test
	public void testClonar() {
		char [][] res = Programa.clonar(te);
		assertArrayEquals(ts,  res);
	}
	

}
