package joc;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ClonarTest.class, EsPredefinitTest.class, IterarTest.class })
public class AllTests {

}
