package joc;
import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
	
@RunWith(Parameterized.class)
public class EsPredefinitTest {
	private char [][] te;
	private String ts;
	

	
	private static char [] [] [] entrada = { 
									{	{' ',' ',' ',' ',' ',' ',' ',' ',' '},
										{' ',' ',' ','V','V','V',' ',' ',' '},
										{' ',' ',' ',' ',' ',' ',' ',' ',' '},
										{' ','V',' ',' ',' ',' ',' ','V',' '},
										{' ','V',' ',' ',' ',' ',' ','V',' '},
										{' ','V',' ',' ',' ',' ',' ','V',' '},
										{' ',' ',' ',' ',' ',' ',' ',' ',' '},
										{' ',' ',' ','V','V','V',' ',' ',' '},
										{' ',' ',' ',' ',' ',' ',' ',' ',' '},
									},
									{ 	{' ',' ',' ',' ',' ',' ',' ',' ',' '},
										{' ',' ',' ',' ',' ',' ',' ',' ',' '},
										{' ',' ','V','V',' ',' ',' ',' ',' '},
										{' ',' ','V',' ',' ',' ',' ',' ',' '},
										{' ',' ',' ',' ',' ','V',' ',' ',' '},
										{' ',' ',' ',' ','V','V',' ',' ',' '},
										{' ',' ',' ',' ',' ',' ',' ',' ',' '},
										{' ',' ',' ',' ',' ',' ',' ',' ',' '},
										{' ',' ',' ',' ',' ',' ',' ',' ',' '},
									},
									{	{' ',' ',' ',' ',' ',' ',' ',' ',' '},
										{' ',' ',' ',' ',' ',' ',' ',' ',' '},
										{' ',' ','V','V','V',' ',' ',' ',' '},
										{' ','V','V','V',' ',' ',' ',' ',' '},
										{' ',' ',' ',' ',' ',' ',' ',' ',' '},
										{' ',' ',' ',' ',' ',' ','V','V','V'},
										{' ',' ',' ',' ',' ','V','V','V',' '},
										{' ',' ',' ',' ',' ',' ',' ',' ',' '},
										{' ',' ',' ',' ',' ',' ',' ',' ',' '},
									},
									{ 	{' ',' ',' ',' ','V',' ',' ',' ',' '},
										{' ',' ',' ',' ','V',' ',' ',' ',' '},
										{' ',' ',' ',' ','V',' ',' ',' ',' '},
										{' ',' ',' ',' ','V',' ',' ',' ',' '},
										{'V','V','V','V','V','V','V','V','V'},
										{' ',' ',' ',' ','V',' ',' ',' ',' '},
										{' ',' ',' ',' ','V',' ',' ',' ',' '},
										{' ',' ',' ',' ','V',' ',' ',' ',' '},
										{' ',' ',' ',' ','V',' ',' ',' ',' '},
									}
								};
	private static String []  sortida = {"uno","dos","tres","no"};
											
	
	public EsPredefinitTest (char [][] t1,String t2) {
		this.te = t1;
		this.ts = t2;
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		
		return Arrays.asList(new Object[][] {
			{entrada[0],  sortida[0]},
			{entrada[1], sortida[1]},
			{entrada[2],  sortida[2]},
			{entrada[3],  sortida[3]}
			
		});
		
	}
	

	@Test
	public void testClonar() {
		String res = Programa.esPredefinit(te);
		assertEquals(ts,  res);
	}
	

}
