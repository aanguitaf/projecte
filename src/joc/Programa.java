package joc;

import java.util.Random;
import java.util.Scanner;

/**
 * Aquesta classe  permet jugar al joc de la vida
 * @author AbelAnguita
 * @version 2.0
 *
 */
public class Programa {
static Scanner src=new Scanner(System.in);
/**
 * Metode main del programma que permet al usuari seleccionar el que vol fer
 * @param args
 */
	public static void main(String[] args) {
		char morta=' ';
		char viva='V';
		char opcio;
		boolean sel=false;
		char[][] tauler= {{'a'}};
		do {
		opcio=MostrarMenu();
		switch(opcio) {
		case '1':
			tauler=inicialitzarTauler(morta,viva);
			sel=true;
			break;
		case '2':
			if(sel) {
			mostrarEstat(tauler);			
			}else {
				System.out.println("Primer inicialitza el tauler!");
			}
			
			break;
		case '3':
			
			if(sel) {
			tauler=iterarTauler(tauler, morta,viva);
			}else {
				System.out.println("Primer inicialitza el tauler!");
			}
			break;
		case '0':
			System.out.println("Gracies per jugar");
			break;
		}
			
		}while(opcio!='0');

	}
	
	/**
	 * Aquest metode demana un nombre d'iteracions i iteraciona el tauler tantes vegades com les seleccionades
	 * @param tauler matriu de caracters que mostra l'estat del joc de la vida
	 * @param morta caracter que simbolitza una celula morta
	 * @param viva caracter que simbolitza una celula viva
	 * @return el tauler després d'haver ser iterat
	 */
	public static char[][] iterarTauler(char[][] tauler, char morta, char viva) {
		String compr=esPredefinit(tauler);
		System.out.println("Introdueix el nombre d'iteracions");
		int iter=obteniritercions();
		src.nextLine();
		for(int i=0;i<iter;i++) {
			limpiarConsola();
			mostrarEstat(tauler);
			tauler=iterar(tauler,i,compr,morta,viva);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
		limpiarConsola();
		mostrarEstat(tauler);
		return tauler;
	}

	/**
	 * Aquest metode deixa un gran espai en la consola per simular que s'esborra
	 */
	public static void limpiarConsola() {
		// TODO Auto-generated method stub
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		
	}

	/**
	 * Aquest metode demana un nombre valid d'iteracions per consola
	 * @return un nombre d'iteracions valid
	 */
	public static int obteniritercions() {
		int n=0;
		boolean bien=false;
		do {
			try {
				n=src.nextInt();
				if(n>=0 && n<=100) {
					bien=true;
				}else {
					System.out.println("Error, ha de ser un nombre entre 0 i 100");
					src.nextLine();
				}
			}catch(Exception e) {
				System.out.println("Error, ha de ser un nombre entre 0 i 100");
				src.nextLine();
			}
			
		}while(!bien);
		return n;
	}

	/**
	 * Aquest metode itera el tauler una vegada tenint en compte si es un 
	 * tauler predefinit o si es un tauler aleatori, si es predefinit selecciona l'estat del
	 * tauler tenint en compte el nombre de l'iteracio
	 * @param tauler matriu de caracters que mostra l'estat del joc de la vida
	 * @param i Integer que indica el nombre de l'iteracio
	 * @param compr String que indica si el tauler es o no predefinit i en el cas que ho sigui quin predefinit es.
	 * @param morta caracter que simbolitza una celula morta
	 * @param viva caracter que simbolitza una celula viva
	 * @return el tauler iterat una vegada
	 */
	public static char[][] iterar(char[][] tauler, int i, String compr, char morta, char viva) {
		char[][] tauler2;
		switch(compr) {
		case "uno":
			char[][]predefinit11= {
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ','V','V','V',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ','V',' ',' ',' ',' ',' ','V',' '},
					{' ','V',' ',' ',' ',' ',' ','V',' '},
					{' ','V',' ',' ',' ',' ',' ','V',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ','V','V','V',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
			};
			char[][]predefinit12= {
					{' ',' ',' ',' ','V',' ',' ',' ',' '},
					{' ',' ',' ',' ','V',' ',' ',' ',' '},
					{' ',' ',' ',' ','V',' ',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{'V','V','V',' ',' ',' ','V','V','V'},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ','V',' ',' ',' ',' '},
					{' ',' ',' ',' ','V',' ',' ',' ',' '},
					{' ',' ',' ',' ','V',' ',' ',' ',' '},
			};
			
			
			if(i%2!=0) {
				tauler=clonar(predefinit11);
			}else {
				tauler=clonar(predefinit12);
			}
			
			break;
		case "dos":

			char[][]predefinit21= {
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ','V','V',' ',' ',' ',' ',' '},
					{' ',' ','V',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ',' ','V',' ',' ',' '},
					{' ',' ',' ',' ','V','V',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
			};
			
			char[][]predefinit22= {
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ','V','V',' ',' ',' ',' ',' '},
					{' ',' ','V','V',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ','V','V',' ',' ',' '},
					{' ',' ',' ',' ','V','V',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
			};
			
			if(i%2!=0) {
				tauler=clonar(predefinit21);
			}else {
				tauler=clonar(predefinit22);
			}
			break;
		case "tres":
			char[][]predefinit31= {
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ','V','V','V',' ',' ',' ',' '},
					{' ','V','V','V',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ','V','V','V'},
					{' ',' ',' ',' ',' ','V','V','V',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
			};
			
			char[][]predefinit32= {
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
					{' ',' ',' ','V',' ',' ',' ',' ',' '},
					{' ','V',' ',' ','V',' ',' ',' ',' '},
					{' ','V',' ',' ','V',' ',' ',' ',' '},
					{' ',' ','V',' ',' ',' ',' ','V',' '},
					{' ',' ',' ',' ',' ','V',' ',' ','V'},
					{' ',' ',' ',' ',' ','V',' ',' ','V'},
					{' ',' ',' ',' ',' ',' ','V',' ',' '},
					{' ',' ',' ',' ',' ',' ',' ',' ',' '},
			};
			if(i%2!=0) {
				tauler=clonar(predefinit31);
			}else {
				tauler=clonar(predefinit32);
			}
			
			break;
		case "no":
			tauler2=new char[tauler.length][tauler[0].length];
			for(int e=0;e<tauler.length;e++)
				for(int j=0;j<tauler[0].length;j++)
					if(tauler[e][j]==morta) {
						int v=0;
						for(int c=e-1;c<=e+1;c++) {
							if(c>=0 && c<tauler.length) {
								for(int f=j-1;f<=j+1;f++) {
									if(f>=0 && f<tauler[0].length) {
										if(tauler[c][f]==viva) {
											v++;
										}
									}
								}
							}
						}
						if(v==3)
							tauler2[e][j]=viva;
						else
							tauler2[e][j]=morta;
					}else {
						int v=-1;
						for(int c=e-1;c<=e+1;c++) {
							if(c>=0 && c<tauler.length) {
								for(int f=j-1;f<=j+1;f++) {
									if(f>=0 && f<tauler[0].length) {
										if(tauler[c][f]==viva) {
											v++;
										}
									}
								}
							}
						}
						if(v==3||v==2)
							tauler2[e][j]=viva;
						else
							tauler2[e][j]=morta;
					}
			tauler=clonar(tauler2);
			break;
			
		}
		
		
		return tauler;
	}
	
	/**
	 * Aquest metode copia en tauler els continguts del tauler iterat
	 * @param tauler2 versio iterada del tauler
	 * @return el tauler després de copiar el contingut de tauler2
	 */
	public static char[][] clonar(char[][] tauler2) {
		char [][]tauler=new char[tauler2.length][tauler2[0].length];
		for(int i=0;i<tauler2.length;i++)
			for(int j=0;j<tauler2[0].length;j++)
				tauler[i][j]=tauler2[i][j];
		return tauler;
	}

	/**
	 * Aquest metode comproba si el tauler es o no predefinit i en el cas de que ho sigui quin predefinit es
	 * @param tauler matriu de caracters que mostra l'estat del joc de la vida
	 * @return String que indica si el tauler es o no predefinit i en el cas que ho sigui quin predefinit es
	 */
	public static String esPredefinit(char[][] tauler) {
		char[][]predefinit1= {
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ','V','V','V',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ','V',' ',' ',' ',' ',' ','V',' '},
				{' ','V',' ',' ',' ',' ',' ','V',' '},
				{' ','V',' ',' ',' ',' ',' ','V',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ','V','V','V',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
		};
		
		char[][]predefinit2= {
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ','V','V',' ',' ',' ',' ',' '},
				{' ',' ','V',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ','V',' ',' ',' '},
				{' ',' ',' ',' ','V','V',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
		};
		
	
		char[][]predefinit3= {
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ','V','V','V',' ',' ',' ',' '},
				{' ','V','V','V',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ','V','V','V'},
				{' ',' ',' ',' ',' ','V','V','V',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
		};
		boolean igual=true;
		if(tauler.length!=predefinit1.length||tauler[0].length!=predefinit1[0].length){
			igual=false;
		}
		int i=0;
		int j=0;
		while(i<predefinit1.length && igual==true) {
			while(j<predefinit1[0].length && igual==true) {
				if(predefinit1[i][j]!=tauler[i][j]) {
					igual=false;
				}
				j++;
			}
			j=0;
			i++;
		}
		if(igual) {
			return "uno";	
		}
		
		igual=true;
		if(tauler.length!=predefinit2.length||tauler[0].length!=predefinit2[0].length){
			igual=false;
		}
		 i=0;
		 j=0;
		while(i<predefinit2.length && igual==true) {
			while(j<predefinit2[0].length && igual==true) {
				if(predefinit2[i][j]!=tauler[i][j]) {
					igual=false;
				}
				j++;
			}
			j=0;
			i++;
		}
		
		if(igual) {
			return "dos";	
		}
		
		igual=true;
		if(tauler.length!=predefinit3.length||tauler[0].length!=predefinit3[0].length){
			igual=false;
		}
		 i=0;
		 j=0;
		while(i<predefinit3.length && igual==true) {
			while(j<predefinit3[0].length && igual==true) {
				if(predefinit3[i][j]!=tauler[i][j]) {
					igual=false;
				}
				j++;
			}
			j=0;
			i++;
		}
		if(igual) {
			return "tres";	
		}
		return "no";
	}

	/**
	 * mostra per pantalla els continguts del tauler
	 * @param tauler matriu de caracters que mostra l'estat del joc de la vida
	 */
	public static void mostrarEstat(char[][] tauler) {
		for(int i=0;i<tauler.length;i++) {
			for(int j=0;j<tauler[0].length;j++)
				System.out.print(tauler[i][j]);
			System.out.println();
		}
		System.out.println();
	}

	/**
	 * Aquest metode permet seleccionar si vols un tauler predefinit o aleatori
	 * @param morta caracter que simbolitza una celula morta
	 * @param viva caracter que simbolitza una celula viva
	 * @return el tauler inicialitzat
	 */
	public static char[][] inicialitzarTauler(char morta, char viva) {
	char[][]tauler= {{'a'}};
	char sel=SeleccionarTauler();
	switch (sel) {
	case '1':
		char[][]predefinit1= {
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ','V','V','V',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ','V',' ',' ',' ',' ',' ','V',' '},
				{' ','V',' ',' ',' ',' ',' ','V',' '},
				{' ','V',' ',' ',' ',' ',' ','V',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ','V','V','V',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
		};
		tauler=predefinit1;
		break;
	case '2':
		char[][]predefinit2= {
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ','V','V',' ',' ',' ',' ',' '},
				{' ',' ','V',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ','V',' ',' ',' '},
				{' ',' ',' ',' ','V','V',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
		};
		tauler=predefinit2;
		break;
	case '3':
		char[][]predefinit3= {
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ','V','V','V',' ',' ',' ',' '},
				{' ','V','V','V',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ','V','V','V'},
				{' ',' ',' ',' ',' ','V','V','V',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' ',' '},
		};
		tauler=predefinit3;
		break;
	case '4':
		char[][]personalitzat=emplenarPersonalitzat(morta,viva);
		tauler=personalitzat;
		break;
	
	}
	return tauler;
	}

	/**
	 * Aquest metode et permet inicialitzar un tauler amb la mida dels costats
	 * personalitzada i un contingut aleatori
	 * @param morta caracter que simbolitza una celula morta
	 * @param viva caracter que simbolitza una celula viva
	 * @return el tauler personalitzat inicialitzat
	 */
	public static char[][] emplenarPersonalitzat(char morta, char viva) {
		Random rdc=new Random();
		System.out.println("Selecciona la mida dels costats del quadrat");
		int costat=obtenirmidacostat();
		src.nextLine();
		char[][] per=new char[costat][costat];
		int vives=rdc.nextInt(costat*costat*1/2-costat*costat*1/4)+costat*costat*1/4;
		
		for(int i=0;i<vives;i++) {
			boolean bien=false;
			do {
				int e=rdc.nextInt(costat);
				int j=rdc.nextInt(costat);
				if(per[e][j]!='V') {
					per[e][j]='V';
					bien=true;
				}
			}while(!bien);
		}
		
		emplenarPersonalitzatEspais(per,morta,viva,costat);
		return per;
	}
	 
	/**
	 * Aquest metode demana un nombre valid de costats
	 * @return un nombre valid de costats
	 */
	public static int obtenirmidacostat() {
		int n=0;
		boolean bien=false;
		do {
			try {
				n=src.nextInt();
				if(n>=4 && n<=10) {
					bien=true;
				}else {
					System.out.println("Error, ha de ser un nombre entre 4 i 10");
					src.nextLine();
				}
			}catch(Exception e) {
				System.out.println("Error, ha de ser un nombre entre 4 i 10");
				src.nextLine();
			}
			
		}while(!bien);
		return n;
	}
	
	
	/**
	 * Omple els espais sense celules vives amb celules mortes
	 * @param per tauler personalitzat amb els empais sense omplir
	 * @param morta caracter que simbolitza una celula morta
	 * @param viva caracter que simbolitza una celula viva
	 * @param costat nombre de costats del tauler 
	 */
	public static void emplenarPersonalitzatEspais(char[][] per, char morta, char viva, int costat) {
		for(int i=0;i<costat;i++)
			for(int j=0;j<costat;j++)
				if(per[i][j]!=viva)
					per[i][j]=morta;
		
	}

	/**
	 * Mostra un menu i permet seleccionar una opcio valida
	 * @return una opcio valida
	 */
	public static char SeleccionarTauler() {
		boolean bien=false;
		char op='e';
		do {
			System.out.println("1.- Predefiit 1");
			System.out.println("2.- Predefinit 2");
			System.out.println("3.- Predefinit 3");
			System.out.println("4.- Selecciona mida i emplena aleatoriament");
			op=src.nextLine().charAt(0);
			if(op=='1'||op=='2'||op=='3'||op=='4') {
			bien=true;
			}else {
				System.out.println("Caracter incorrecte");
			}
		}while(!bien);
		return op;
	}

	/**
	 * Mostra un menu i permet seleccionar una opcio valida
	 * @return una opcio valida
	 */
	public static char MostrarMenu() {
		boolean bien=false;
		char op='e';
		do {
			System.out.println("1.- Inicialitzar tauler");
			System.out.println("2.- Visualitzar tauler");
			System.out.println("3.- Iterar");
			System.out.println("0.- Sortir");
			op=src.nextLine().charAt(0);
			if(op=='1'||op=='2'||op=='3'||op=='0') {
			bien=true;
			}else {
				System.out.println("Caracter incorrecte");
			}
		}while(!bien);
		return op;
		
	}
}
